<!doctype html>
<html lang="hu">
 <head>
 <meta charset="utf-8">
  <title>Első PHP fileom</title>
 </head>
 <body>
Helo Világ!
<?php
//egysoros komment
#egysoros komment
/*
többsoros 
komment
*/
echo "Hello PHP";//minden utasítás végén kötelező a ; | operátor: "" vagy '' a határolt értéket szöveges értékké alakítja (string) | echo kiírás a standard outputra (stdo)
print '<br>Hello Again';


//nevünk kiírása h3as TAGben
echo '<h3>Horváth György</h3>';

//egy divbe írjuk ki az aktuális dátumidőt
//tároljuk el egy változóba
$now = date("Y-m-d H:i:s");//változónév: nem kezdődhet számmal, case sensitive, nem lehet benne speciális karakter, kivéve: _ 
//operátor: = -> értékadó operátor (assignment)
echo '<div>';
echo $now;
echo '</div>';

sleep(1);//állj 1 mp ig

echo '<div>';
echo $now;
echo '</div>';

//Értékek, tipusok
//Egyszerű értékek vagy Primitívek
$id = 231;//tipus: egész szám (integer vagy int)
$price = 299.50;//lebegő pontos szám (floating point number vagy float)
$isMichaelJacksonAlive = false; //logikai igen nem, igaz-hamis,1-0 (boolean vagy bool)
$is_billy_idol_alive = true; 
$name="Horváth György";//szöveges érték (string)

//fejlesztés közben információ kérése a változóról
//pre előformázó tag nyitása
echo '<pre>';
var_dump( $now , $id );
var_dump($price, $is_billy_idol_alive);
echo '</pre>';

//PHP HyperText Preprocessor

?>
 </body>
</html>