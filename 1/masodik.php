<?php
//pure PHP file
$a = 2;
$b = 30;
//mennyi az átfogó ha a befogó $a és $b
$c = sqrt($a*$a + $b*$b);
//válasz megfogalmazása és kiírása
/*
echo 'Egy 3m és 4m befogójú háromszög átfogója: ';
echo $c;
echo 'm';
*/
//operátor: konkatenáció - összefűzés -> .
echo 'Egy ' . $a . 'm és ' . $b .'m befogójú háromszög átfogója: ' . $c . 'm.';

echo "<br>Egy {$a}m és $b m  befogójú háromszög átfogója: $c m";
echo "<br>Egy {$a}m és \$b m  befogójú háromszög átfogója: $c m";//operátor: \ ->escape : kilépteti a következő karaktert a nyelvi elemek közül
$test = $a . $b;
var_dump($a,$b,$test/11);


