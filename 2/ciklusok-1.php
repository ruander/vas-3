<?php
//a FOR ciklus
/*
for(ciklusváltozó kezdeti értéke; belépési feltétel vizsgálata; ciklusváltozó léptetése){
    //ciklusmag
}
 */

#írjuk ki új sorba az egész számokat 1-10ig
for ($i = 1; $i <= 10; $i++ ) {//operátor $i++ -> $i = $i + 1 ; $i = $i + 3 -> $i += 3 | analógia -=,/=, *=, **=, .=
    echo "<br>$i";
}
echo "<br>!!!!!".$i;//i 11 és ezért jutottunk eddig a sorig mert 11 nem kisebb vagy egyenlő mint 10, nem hajtottuk többször végre a ciklusmagot

//TÖMB bemutatása
$array = [];//üres tömb
//var_export(változó,stringként térjen e vissza)
var_dump($array,$i);
echo '<pre>'.var_export($array,true).'</pre>';
#echo '<pre>'.var_export($i,true).'</pre>';
$array = [1,2,3,'hello',true, pi()];//tömb megadása automatikus indexen értékek halmozásával
echo '<pre>'.var_export($array,true).'</pre>';
//tömb egy elem kiolvasása:
echo $array[3];
////tömb egy elem megadása/felülírása:
$array[100] = 'ez a 100as index';
echo '<pre>'.var_export($array,true).'</pre>';
//nem létező index: (hiba)

//ha létezik az index vizsgálat
if( array_key_exists(10,$array) ){
    echo $array[10];
}

//tömb bővítése 1 elemmel automatikus indexen
$array[] = 'teszt';
echo '<pre>'.var_export($array,true).'</pre>';

//tömb elemszáma
echo $quantity = count($array);
//for ciklussal
for($i = 0; $i < 7; $i++){
    echo "<br>index: $i érték: ".$array[$i];
}
//tömb bejárása foreach ciklussal
/*
 foreach(tömb as index => érték){
    ciklusmag ahol az aktuális index és érték elérhető
}
 */
foreach($array as $key => $value){
    echo "<br>index: $key érték: ".$value;
}

/**
 * @todo
 -gyakorlas_txt fileból 1-11 ig
 -gyakorlas_txt fileból 14-19 ig
 -feladatgyujtemény pdf ből az első 5 feladat
 */
