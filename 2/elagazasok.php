<?php

#az IF elágazás
/*
if(condition[true|false]) {
    //igaz ág
}else{
    //hamis ág
}
 */

$c = true;

if ($c) {
    echo '$c értéke igaz';
} else {
    echo '$c értéke hamis';
}

$random_number = rand(1, 100);

echo '<br>' . $random_number;
//írjuk hogy a generált szám páros vagy páratlan

if ($random_number % 2 !== '0') {
    echo ', ami páratlan.';
} else {
    echo ', ami páros.';
}
/*
 operátorok vizsgálathoz
$a == $b | érték egyenlőség vizsgálat !!!! (ha a és b egyezik true, egyébként false)
$a != $b | érték egyenlőtlenség vizsgálat !!!! (ha a és b nem egyezik true, egyébként false)
$a < $b
$a <= $b
$a > $b
$a >= $b
-----------
 */
echo '<pre>';
var_dump(1 == 1);
var_dump(1 == '1');
var_dump(1 == true);
var_dump(0 == false);
#megoldás-> strict vagy feszes vizsgálat (érték ÉS TIPUS egyezésre vagy nem egyezésre
var_dump(1 === 1);
var_dump(1 === '1');
var_dump(1 === true);
var_dump(0 === false);
var_dump(0 !== false);
echo '</pre>';
/*
 Több ág

if(condition1){
    //condition1 === true
}elseif(condition2){
    //condition1 === false de condition2 === true
}else{
    se condition1 se condition2 nem igaz
}

Elágazás akárhány ága is van, egyszerre csak 1 ág hajtódik végre
 */
/**
 * @todo Órai feladat.
 * a) írd ki h2 es címsorba az oldal lekérésekor kiolvasható dátumidőt
 * b) a másodpercek alapján a kiírás színe legyen piros, ha páros másodperc és kék, ha páratlan másodperc volt éppen.
 */
$time = time();//ennyi az idő (UNIX TIMESTAMP)
$now = date('Y-m-d H:i:s', $time);
$second = date('s', $time);
//másodperc páros-e
$color = 'blue';
if ($second % 2 === 0){
    $color = 'red';
}
echo '<h2 style="color:'.$color.' ;">' . $now . '</h2>';
