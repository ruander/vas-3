<?php

#1. Írjon egy php programot, amely kiszámolja és kiírja a 2 m élhosszúságú kocka felületét.
$a = 2;
$surface = $a**2 * 6;

echo 'Egy '.$a.'m élhosszúságú kocka felszíne ' . $surface . 'm<sup>2</sup>';

$v = pow($a,3);//$a**3;//$a*$a*$a;operátor: ** ->hatványozás

echo ", a térfogata {$v}m<sup>3</sup>";
