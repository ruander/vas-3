<?php
//8. Készítsünk programot, amely bekér egy N természetes számot, majd
//kirajzol a képernyőre egymás mellé N-szer az "XO" betűket és a kiírás
//után a kurzort a következő sor elejére teszi.

if( !empty($_POST) ) {
    $error = false;//itt lesz a hiba ha van
    var_dump($_POST);
    $N = filter_input(INPUT_POST,'N',FILTER_VALIDATE_INT);

    if( !$N || $N < 1 ){//ha nincs N VAGY 1 nél kisebb N-t adtak meg
        $error = '<span class="error">Nem értelmezhető adat!</span>';
    }

    if(!$error){
        //die('ok');
        //ideiglenesen itt írunk ki a doctype előtt ...
        $char = 'XO';
        //...echo str_repeat($char,$N).'<br>';
        for($i=1;$i<=$N;$i++){
            echo $char;
        }
        echo '<br>';
    }


}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Űrlap feldolgozása</title>
</head>
<body>
Feladatgyűjtemény.pdf feladatok:
<form method="post">
    <label>
        <span>Adj meg egy pozitív egész számot</span>
        <input type="text" name="N" placeholder="1-100" value="<?php echo filter_input(INPUT_POST, 'N'); ?>">
        <?php
        echo $error ?? '';//'hibakiírás ha van'
        ?>
    </label>
    <br>
    <button>Mehet</button>
</form>
<!--@todo HF: a cloudon lévő .txt file összes (1-24), ÉS! a feladatgyüjtemény.pdf 1-10. feladatok -->
</body>
</html>

