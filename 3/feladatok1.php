<?php

//21.Írjon egy programot, amelyben egy 10 elemű tömböt tetszőleges értékekkel tölt fel, majd kiírja a tömb legkisebb és legnagyobb elemét.
$arr = [];//ebbe gyűjtjük az elemeket

for ($i = 1; $i <= 10; $i++) {
    $arr[] = rand(1, 100);//tegyünk a tömbbe egy véletlen egész számot
}


$min = min($arr);
$max = max($arr);
echo '<pre>';
var_dump($arr, $min, $max);

/**
 * A while ciklus
 * while(belépési feltétel vizsgálat){
 * //ciklusmag
 * }
 */
//21. feladat while ciklussal
$arr = [];//tömb újra üres
while (count($arr) < 10) {
    $arr[] = rand(1, 100);
}
$min = min($arr);
$max = max($arr);

var_dump($arr, $min, $max);

/*
 hátultesztelő változat
do{
    //ciklusmag
}while(belépési feltétel)
 */
//a feladat 2 elvi megoldása cserélt megvalósítással
$arr = [];
$i = 1;//ciklusváltozó
while ($i <= 10) {//belépési feltétel
    //ciklusmag
    $arr[] = rand(1, 100);
    //ciklusváltozó növelése
    $i++;
}
$min = min($arr);
$max = max($arr);

var_dump($arr, $min, $max);

//while megoldás for-ral
$arr = [];//tömb ürítése
for (; count($arr) < 10;) {
    $arr[] = rand(1, 100);//tegyünk a tömbbe egy véletlen egész számot
}
$min = min($arr);
$max = max($arr);

var_dump($arr, $min, $max);
echo '</pre>';
//22. Írjon egy programot, amelyben egy 10 elemű tömböt tetszőleges értékekkel tölt fel, majd kiírja, hogy a tömbben hány darab olyan szám van, amely nagyobb, mint 10 és kisebb 20.
$arr = [];//tömb újra üres
while (count($arr) < 10) {
    $arr[] = rand(1, 30);
}
echo 'A tömb elemei: ' . implode(',', $arr);
//járjuk be a tömböt és gyüjtsük össze egy másik tömbbe azokat az elemeket amik 10 és 20 közé esnek
$result = [];
$db = 0;//ennyi darab
foreach ($arr as $v) {
    if ($v < 20 && $v > 10) {//mindkét feltételnek egyszerre igaznak kell lennie
        $result[] = $v;
        $db++;
    }
}
echo "<br>$db";
//resultos megoldás kiírása
$result_nr = count($result);
echo '<br>10 és 20 közé eső elemek száma: ' . $result_nr;
//növekvő sorrend
sort($result);
//ha van 0 nál több kiírjuk őket
if ($result_nr > 0) {
    echo '<br>ezek: ' . implode(',', $result);
}

//23.Írjon egy programot, amelyben egy 10 elemű tömböt tetszőleges értékekkel tölt fel, majd a tömb elemeit átmásolja egy másik tömbbe úgy, hogy az elemek fordított sorrendben helyezkedjenek el benne.
$arr = [];//tömb újra üres
while (count($arr) < 10) {
    $arr[] = rand(1, 30);
}
echo '<pre>' . var_export($arr, true) . '</pre>';
$reverse = array_reverse($arr);
echo '<pre>' . var_export($reverse, true) . '</pre>';

//24.Írjon egy programot, amelyben egy 10 elemű tömböt tetszőleges értékekkel tölt fel, amelyek valamilyen termékek nettó árai, majd számolja ki és írassa ki a termékek bruttó árait, ha az ÁFA 20 %.
$arr = [];//tömb újra üres
while (count($arr) < 10) {
    $arr[] = rand(100, 9999);
}
echo '<pre>' . var_export($arr, true) . '</pre>';
$vat = 20;//áfa 20%

foreach ($arr as $net) {
    $gross = $net * (1 + $vat / 100);
    echo '<br>netto: ' . $net . ' | brutto: ' . $gross . ' | kerekítve: ' . floor($gross) ;
}

/*
19.Írjon egy programot ciklus utasítást használva, amely az alábbi elrendezésű szöveget írja ki a képernyőre:
2
22
222
2222
*/
$char = '2';
for($i=1;$i<=4;$i++){//sorok
    echo '<br>';
    //belső ciklus a 2esek kiírására
    for($j=1;$j<=$i;$j++){
        echo $char;
    }
}
for($i=1;$i<=4;$i++){//sorok
    echo '<br>'.str_repeat($char,$i);
}

/*
20.Írjon egy programot ciklus utasítást használva, amely az alábbi elrendezésű szöveget írja ki a képernyőre:
2222
222
22
2
 */
for($i=4;$i>0;$i--){//sorok
    echo '<br>';
    //belső ciklus a 2esek kiírására
    for($j=1;$j<=$i;$j++){
        echo $char;
    }
}
