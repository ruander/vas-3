<?php
$test = [1, 24, true, 'valami', 7 / 6];
//$test = array(1, 24, true, 'valami', 7 / 6);
echo '<pre>' . var_export($test, true) . '</pre>';
//redeclare,override
//tömb elemeinek megadása (felülírása) index megadással
$test = [
    0 => 10,
    10 => 'valami',
    27 => '...teszt'
];
$test[100] = '100 as index';
echo '<pre>' . var_export($test, true) . '</pre>';

//asszociatív tömb
$test['szoveg-index'] = 'ez egy asszociativ indexen lévő érték';
echo '<pre>' . var_export($test, true) . '</pre>';
echo $test['szoveg-index'];

//bejárás foreach ciklussal
foreach ($test as $key => $value) {
    echo "<br>index: $key, érték: $value ";
}
/*$test = [1,2,3,4];
list($a,$b,$c,$d) = $test;
var_dump($a,$b,$c,$d);*/
//$test = ['a', 'b', 'c', 'd'];
/* 20 => 'b',
 'test' => 10
];*/
[$x, $y, $z] = $test;
var_dump($x, $y, $z);

//asszociatív tömbök a gyakorlatban
$user = [
    'id' => 1,
    'name' => 'Gyuri',
    'email' => 'hgy@iworkshop.hu'
];
//kiírás is beszédes tud lenni így

echo "<br>Név: {$user["name"]} | &lt;{$user['email']}&gt;";

echo '<pre>';
//csak értékek
var_dump(array_values($user));
//csak kulcsok
var_dump(array_keys($user));
//kulcs-érték csere
var_dump(array_flip($user));
//sorrendezés 1 (belenyul a tömbbe)
//echo $test[27];
sort($test);//&array -> referenciát ad át a műveletre
var_dump($test);
//echo $test[27];
