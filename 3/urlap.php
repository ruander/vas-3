<?php

//POST feldolgozás, ha van mit
if( empty($_POST) === false ) {
    echo '<pre>';
    var_dump($_POST);
    echo '</pre>';

    //echo $_POST['test'];//!!!!!!!!!Ezt ÍGY SOHA se a a getből, se a postból!!!!!!!!!!!!
    //helyette MINDIG !!!!!!!!! filter_input()
    $test = filter_input(INPUT_POST, 'test',FILTER_VALIDATE_INT);
    var_dump($test);
    if($test === false){//hiba esetén hibaüzenet tárolása
        $error = 'Nem szám lett beírva!';
    }

    if( isset($error) === false ){//ha nincs hiba, jó adatot kaptunk
        die('ok');//exit()
    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Űrlap feldolgozása</title>
</head>
<body>
<h1>Űrlapok</h1>
<h2>Get metódus</h2>
<form method="get" action="process.php">
    <label>
        <span>Text input</span>
        <input type="text" name="test">
    </label>
    <button>Mehet</button>
</form>
<h2>Post metódus</h2>
<form method="post"><!--action="process.php?test=5"-->
    <label>
        <span>Text input</span>
        <input type="text" name="test" placeholder="1-100" value="<?php echo filter_input(INPUT_POST,'test'); ?>">


        <?php
        /*if( isset($error) ){
            echo $error; //hibaüzenet kiírása, ha van
        }*/

        echo $error ?? '';//'hibakiírás ha van' új tipusú megoldása nullsafe (??) operátorral

        ?>
    </label>
    <br>
    <button>Mehet</button>
</form>
</body>
</html>
