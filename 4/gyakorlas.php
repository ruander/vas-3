<?php
var_dump( filter_input(INPUT_GET,'page',FILTER_VALIDATE_INT) );//szuperglobális GET (url paraméter) csak egész szám lehet
//1. Készítsen egy olyan ciklust, amely egymás után menü feliratokat rak ki.
/*for($i=1;$i<=5;$i++){
    echo 'menu';
}*/
$mainMenu = [
    1 => [
        'title'=>'home',
        'menuIcon'=>'fa fa-home',
        'color' => 'orange'
    ],
    2 => [
        'title'=>'about',
        'menuIcon'=>'fa fa-user',
        'color' => 'purple'
    ],
    3 => [
        'title'=>'services',
        'menuIcon'=>'fa fa-services',
        'color' => 'yellow'
    ],
    4 => [
        'title'=>'contact',
        'menuIcon'=>'fa fa-envelope',
        'color' => 'pink'
    ],
];//menüpontok tömbje
//
$menuHTML = '<nav><ul>';//menüelemek nyitása

//menüpontok
foreach ($mainMenu as $menuID => $menuItem) {
    $menuHTML .= '<li 
                    class="' . $menuItem['menuIcon'] . '" 
                    style="background:' . $menuItem['color'] . '"
                    ><a href="?page=' . $menuID . '">' . $menuItem['title'] . '</a></li>';//menüpont kialakítása és hozzáfűzése a végén kiírandó elemhez
}

//elemek zárása
$menuHTML .= '</ul></nav>';//operátor: $a = $a . 'valami' -> $a .= 'valami'; analógia> += -= *= /=

//kiírás 1 lépésben
echo $menuHTML;

/*
2. Készítsünk programot, amely kiszámolja az első 100 darab természetes szám összegét, majd kiírja az eredményt. (Az összeg kiszámolásához vezessünk be egy változót, amelyet a program elején kinullázunk, a ciklusmagban pedig mindig hozzáadjuk a ciklusváltozó értékét, tehát sorban az 1, 2, 3, 4, ..., 100 számokat.)
 */
$sum = 0;
for($i=1;$i<=100;$i++){
    $sum += $i; //$sum = $sum + $i;
}
echo '<br>'.$sum;

/*
3. Készítsünk programot, amely kiszámolja az első 7 darab. természetes
szám szorzatát egy ciklus segítségével. (A szorzat kiszámolásához
vezessünk be egy változót, amelyet a program elején beállítunk 1-re, a
ciklusmagban pedig mindig hozzászorozzuk a ciklusváltozó értékét,
tehát sorban az 1, 2, 3, ..., 7 számokat.)
 */
$mtpl = 1;
for($i=1;$i<=7;$i++){
    $mtpl *= $i;
}

echo '<br>'.$mtpl;

/*4. Készítsünk programot, amely kiszámolja az első 100 darab páros szám
összegét (A ciklus vegyük egytől ötvenig, majd a ciklusmagban vegyük a
ciklusváltozó kétszeresét - így megkapjuk a páros számokat. Ezeket
hasonlóan adjuk össze, mint az első feladatban).*/

$sum = 0;
for($i=1;$i<=50;$i++){
    $sum += $i*2; //$sum = $sum + $i;
}
echo '<br>'.$sum;

/*5. Készítsünk programot, amely kiszámolja az első 100 darab. páratlan
szám összegét (A ciklus vegyük egytől ötvenig, majd a ciklusmagban
vegyük a ciklusváltozó kétszeresét eggyel csökkentve - így megkapjuk a
páratlan számokat. Ezeket hasonlóan adjuk össze, mint az első
feladatban).*/
$sum = 0;
$even = 0;//csak páros
$odd = 0;//csak páratlan
for($i=1;$i<=100;$i++){
    $sum += $i; //$sum = $sum + $i;

    if( $i%2 === 0  ){//ha páros
        $even += $i;
    }

}
echo '<br>'.$sum;
echo '<br>Páros:'.$even;
echo '<br>Páratlan:'. ($sum - $even);
