<?php
$output = '';   //Erőforrás a kiíráshoz
/*
 8. Készítsünk programot, amely bekér egy N természetes számot, majd
kirajzol a képernyőre egymás mellé N-szer az "XO" betűket és a kiírás
után a kurzort a következő sor elejére teszi.
 */
$output .= '<p>8. Készítsünk programot, amely bekér egy N természetes számot, majd
kirajzol a képernyőre egymás mellé N-szer az "XO" betűket és a kiírás
után a kurzort a következő sor elejére teszi.</p>';
//ha van POST adat,dolgozzuk fel
if (!empty($_POST)) {
    var_dump($_POST);
    $errors = [];//ide gyűjtjük a hibaüzeneteket(hibákat)

    //adatfeldolgozás/hibakezelés
    $N = filter_input(INPUT_POST, 'N', FILTER_VALIDATE_INT);
    //ha $N NULL vagy false, akkor hibás az adat
    /*if( $N === false || $N === NULL ){// 0,NULL,false
        $errors['N'] = '<span class="error">Nem megfelelő formátum!</span>';
    }*/
    /*    if( !$N /!*$N == false*!/ ){//operátor ! -> negálás
        $errors['N'] = '<span class="error">Nem megfelelő formátum!</span>';
    }*/
    if ($N < 1) {//1 nél kisebb értelmezhetetlen
        $errors['N'] = '<span class="error">Nem megfelelő formátum!</span>';
    }

    if (empty($errors)) {//ha üres maradt a hibatömb hibakezelések után akkor az adatok jók
        //adatok rendberakása...
        $string = 'XO';
        $output .= str_repeat($string, $N) . '<br>';//outputhoz fűzzük a megoldást, most str_repeattel
        //készítsd el while ciklussal
        /*$i=1;
        while($i<=$N){
            $output .= $string;
            $i++;
        }
        $output .= '<br>';*/


        /*
        9. Egészítsük ki az előző programunkat úgy, hogy az előző kiírás alá írja ki
N-szer az "OX" betűket is egymás mellé, majd a kurzort ismét a
következő sor elejére tegye. (Az előző ciklus után - NE bele a ciklusba! -
tegyünk egy hasonló ciklust, ami most XO helyett OX betűket ír ki.)
         */
        $output .= '<p>9. Egészítsük ki az előző programunkat úgy, hogy az előző kiírás alá írja ki 
N-szer az "OX" betűket is egymás mellé, majd a kurzort ismét a 
következő sor elejére tegye. (Az előző ciklus után - NE bele a ciklusba! -
tegyünk egy hasonló ciklust, ami most XO helyett OX betűket ír ki.)</p>';
        //var_dump( (array) $string );//tipuskényszerítés (typecasting)
        $string = 'OX';
        $output .= str_repeat($string, $N) . '<br>';


        $output .= '<p>10.Egészítsük ki a programunkat úgy, hogy az előző két sort N-szer 
ismételje meg a program. (Az előző két egymás utáni ciklust tegyük bele 
egy külső ciklusba.</p>';
        $string1 = 'XO';
        $string2 = 'OX';

        /*$output .= str_repeat(
                str_repeat($string1, $N) . '<br>' . str_repeat($string2, $N) . '<br>'
                ,$N);*/

        for ($i = 1; $i <= $N; $i++) {
            $output .= str_repeat($string1, $N). '<br>';//8/while
            $output .= str_repeat($string2, $N). '<br>';//9/for
        }
        //sortörés
        $output .= '<br>';
    }
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Űrlap feldolgozás/gyakorlás</title>
    <style>
        label {
            display: flex;
            flex-direction: column;
        }

        .error {
            padding: 5px 0;
            color: red;
            font-size: .8em;
            font-style: italic;
        }
    </style>
</head>
<body>
<form method="post">
    <label>
        <span>Adj meg egy N pozitív egész számot</span>
        <input type="text" name="N" placeholder="42" value="<?php echo filter_input(INPUT_POST, 'N'); ?>">
        <?php
        //hiba kiírása ha van
        /*if( isset($errors['N']) ){
            echo $errors['N'];
        }*/
        echo $errors['N'] ?? '';
        ?>
    </label>
    <button>Mehet</button>
</form>
<section>
    <h2>Megoldás</h2>
    <?php
    //megoldás kiírása
    echo $output;
    ?>
</section>
</body>
</html>
