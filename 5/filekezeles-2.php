<?php
$data = [
    [
        'id' => 1,
        'email' => 'hgy@iworkshop.hu',
        'name' => 'Horváth György',
        'time_created' => date('Y-m-d H:i:s')
    ],
    [
        'id' => 2,
        'email' => 'test@user.com',
        'name' => 'Nameless One',
        'time_created' => date('Y-m-d H:i:s')
    ]
];//az adatok amiket tárolni szeretnénk

echo '<pre>' . var_export($data, true) . '</pre>';
//az adatokat konvertáljuk fileba írható formába
echo implode(',', $data);
//implode
$dataImplode = implode(',', $data);

$dir = 'upload/';//ebbe a mappába szeretnénk tárolni
if (!is_dir($dir)) {//ha nem létezik, hozzuk létre
    mkdir($dir, 0755, true);
}

$fileName = 'data.txt';//ebbe a fileba szeretnénk tárolni az adatokat
file_put_contents($dir . $fileName, $dataImplode);
//////////////////////////////
/// visszaolvasás
//ha létezik a file, olvassuk be a tartalmát
if (file_exists($dir . $fileName)) {
    $fileContent = file_get_contents($dir . $fileName);
    echo $fileContent;
    //alakítsuk vissza
    $dataFromFile = explode(',', $fileContent);
    echo '<pre>' . var_export($dataFromFile, true) . '</pre>';

    //hasonlítsuk össze az eredeti adatokat a visszaolvasott adatokkal
    echo 'A két tömb ';
    if ($data === $dataFromFile) {
        echo 'egyezik.';
    } else {
        echo 'nem egyezik.';
    }

}

//Átalakítás sorozattá
$dataSerialize = serialize($data);
echo '<br>' . $dataSerialize;
file_put_contents($dir . $fileName, $dataSerialize);
///////////////////////////////////
/// visszaolvasás és feldolgozás
if (file_exists($dir . $fileName)) {
    $fileContent = file_get_contents($dir . $fileName);
    $dataFromFile = unserialize($fileContent);

    echo '<pre>' . var_export($dataFromFile, true) . '</pre>';
    //hasonlítsuk össze az eredeti adatokat a visszaolvasott adatokkal
    echo 'A két tömb ';
    if ($data === $dataFromFile) {
        echo 'egyezik.';
    } else {
        echo 'nem egyezik.';
    }
}

//json adat javascript object notation
$dataJSON = json_encode($data);
echo '<pre>' . $dataJSON;
//most új filenévvel írjuk ki az adatokat
$fileName = 'data.json';

file_put_contents($dir . $fileName, $dataJSON);

//////////////////////////
/// visszaolvasás és feldolgozás
if (file_exists($dir . $fileName)) {
    $fileContent = file_get_contents($dir . $fileName);
    $dataFromFile = json_decode($fileContent, true);

    echo '<pre>' . var_export($dataFromFile, true) . '</pre>';
    //hasonlítsuk össze az eredeti adatokat a visszaolvasott adatokkal
    echo 'A két tömb ';
    if ($data === $dataFromFile) {
        echo 'egyezik.';
    } else {
        echo 'nem egyezik.';
    }

}

///CSV
$fileName = 'data.csv';

$handle = fopen($dir . $fileName, 'w');
//írjuk ki az adatokat a fileba (2 dimenziós tömb esetén)
foreach($data as $k => $v){
    fputcsv($handle, $v, ';');
}

fclose($handle);

///////////////////////
/// visszaolvasás és feldolgozás (több soros csv file esetén)
if (file_exists($dir . $fileName)) {
    $handle = fopen($dir . $fileName, 'r');

    $dataFromFile = [];//ide olvassuk be az adatokat
    while( ($line = fgetcsv($handle, separator: ';')) !== false ){
        $dataFromFile[] = $line;//uj adatsor hozzáadása
    }


    echo '<pre>' . var_export($dataFromFile, true) . '</pre>';
    //hasonlítsuk össze az eredeti adatokat a visszaolvasott adatokkal
    echo 'A két tömb ';
    if ($data == $dataFromFile) {
        echo 'egyezik.';
    } else {
        echo 'nem egyezik.';
    }

}
/**
 * @todo Megmutatni hogyan lehet eltárolni csv file 1 sorában az asszociatív kulcsokat, majd visszaolvasáskor alkalmazni
 */

/**
 * @todo HF:
 * Készíts egy hibakezelt űrlapot: Név, email, jelszó
 * jelszó legyen elkódolva password_hash használatával tárolás előtt
 * Egy plusz adat is legyen tárolva: time_created, ami a kitöltés ideje
 * Tárold el a kapott adatokat az upload/user.json fileba
 * @todo órai read.php elkészítése
 * Visszaolvasó file [read.php]: ami ha létezik a upload/user.json file akkor egy táblázatba beolvasod a tartalmát (<table>) használj címsort az adat nevének(asszociatív kulcsok)
 *          (- miután tömbbé alakítod elérhető lesz az asszociatív tömb amiből egy array_keys(tömb)
 visszadja csak a kulcsokat egy nem asszociatív tömbben),
 *mivel tudjuk az adatstruktúrát, ezért csak 1 elemünk lehet most így azt bejárva (foreach k=>v)
 *NEM  ECHO hanem $table =
 *table
 *foreach(){
    table .= cimsor + adatsor
 }
 * /table
 *
 *

Doctype és html legyen (table kialakítása doctype előtt
 *
 * table kiírása body ban-> echo table
kész! üzenet ha kész
utána olvasni:
 pcloud sql jegyzet
 */


