<?php
//fileok kezelése PHP val
//magic constansok
$targetDir = 'test/subfolder/';
//mappa létének vizsgálata
var_dump( is_dir($targetDir) );

//mappa létrehozása, ha nem létezik //is_dir -> csak mappa
/*if( is_dir($targetDir) === false ){
    mkdir( $targetDir , 0755, true);
}*/
if( !is_dir($targetDir) ){
    mkdir( $targetDir , 0755, true);
}

$dir = scandir(__DIR__);// . ->jelenlegi mappa, .. -> egyel feljebb mappa + mappák fileok egy 1 dimenziós tömbben

echo '<pre>';
var_dump($dir);

$fileName = 'test.txt';
var_dump(is_file($fileName));//csak file

var_dump(file_exists($fileName));//akár mappa, akár file, ha létezik, true
echo '</pre>';

//mappa törlése (csak üres mappát lehet törölni):
rmdir($targetDir);
rmdir('test/');

//ha létezik a file, írjuk ki a méretét
if(file_exists($fileName)){
    echo 'A file mérete: '.filesize($fileName) . ' Byte';

    $content = 'Ez az új file-tartalom...';
    //tartalom kiírása file-ba
    file_put_contents($fileName, $content);

    //cache törlése
    clearstatcache();
    echo '<br>A file mérete: '.filesize($fileName) . ' Byte';


}else{
    echo 'Nincs ilyen file: '.$fileName;
}


//filetartalom beolvasása
$fileContent = file_get_contents($fileName);
echo '<br>' . $fileContent;

/**
 * @todo fopen(), fread(), fwrite(), fclose(), unlink() átnézni a php.net-ről
 *
 * olvasd be a fileName en található file tartamát az említett fxxxx() eljárások használatával, esetleg egészítsétek ki a file tartalmát egy új sornyi szöveggel
 */
