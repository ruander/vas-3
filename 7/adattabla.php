<?php
require 'connect.php';//db kapcsolat felépítése
/** @var mysqli $link */
//19. Alkalmazott teljes neve, főnöke teljes nev , ha nincs neki akkor 'Boss'
$qry = "SELECT 
            CONCAT(e.firstname,' ',e.lastname) 'Alkalmazott neve',
            IF(
                e.reportsto IS NULL,
                'BOSS',
                CONCAT(e2.firstname,' ',e2.lastname)
            ) 'Főnök neve' 
        FROM employees e
        LEFT JOIN employees e2
        ON e.reportsTo = e2.employeeNumber;";
$result = mysqli_query($link,$qry) or die(mysqli_error($link));

$table = '<table border="1">
            <tr>
             <th>Alkalmazott neve</th>
             <th>Főnök neve</th>
            </tr>';
//adathalmaz ami lehet üres ,1 elemű vagy sok elemű
while( $row = mysqli_fetch_row($result) ){
    //row adatok
    //echo '<pre>'.var_export($row,true).'</pre>';
    $table .= '<tr>
                 <td>'.$row[0].'</td>
                 <td>'.$row[1].'</td>
               </tr>';
}
$table .= '</table>';
echo $table;
