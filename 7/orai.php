<?php
//Adatbázis elérés kialakítása
/*
$dbHost = 'localhost';//adatbázis szerver ip vagy domain általában localhost
$dbUser = 'root';//xamp teljes jogú felh.
$dbPassword = '';
$dbName = 'classicmodels';//ebbe az adatbázisba szeretnénk dolgozni

//kapcsolat felépítése (mysqli procedurális módszerrel) - erőforrás amin tudsz futtatni mysqli_ parancsokkal sql kéréseket
$link = @mysqli_connect($dbHost , $dbUser, $dbPassword, $dbName) or die(mysqli_connect_error().'<br>Keresse a rendszergizdát, mert baj van!');*/
require 'connect.php';/** @var mysqli $link - db kapcsolat mostmár külön file-ból*/
//var_dump($link);
/*$qry = "SELECT * FROM offices;";
//kérés futtatása az adatbázist reprezentáló $link-en -> ez egy eredményhalmazt ad legyen ez $result

$result = mysqli_query($link,$qry) or die(mysqli_error($link));
//var_dump($result);
//eredmény feldolgozása
//1 sor kibontása -> tömb
$row = mysqli_fetch_row($result);
echo '<pre>';
var_dump($row);
echo '</pre>';
//1 sor kibontása -> asszociatív tömb (aminek a kulcsa case sensitiv, azaz s != S)
$row = mysqli_fetch_assoc($result);
echo '<pre>';
var_dump($row);
echo '</pre>';
//1 sor kibontása -> asszociatív tömb+ auto index tömb (2x van benne minden)
$row = mysqli_fetch_array($result);
echo '<pre>';
var_dump($row);
echo '</pre>';

//1 sor kibontása -> objektum
$row = mysqli_fetch_object($result);
echo '<pre>';
var_dump($row);
echo '</pre>';

//maradék elemek kibontása most fetch all-al -> 2. dimenziós tömb, 1dim-> record 2.dim-> mezők
$row = mysqli_fetch_all($result);
echo '<pre>';
var_dump($row);
echo '</pre>';

var_dump(mysqli_fetch_object($result));//ha már nincs elem akkor NULL*/

//sql kérések feladatai
//11. 	Milyen termék méretarányok vannak?
$qry = "SELECT productscale FROM products GROUP BY productscale ORDER BY productscale";
$result = mysqli_query($link,$qry) or die(mysqli_error($link));

//kibontás ciklusban -> listába írás

$output = '<ul>';//lista nyitása
while( ($row = mysqli_fetch_row($result)) !== NULL ){

    $output .='<li>'.$row[0].'</li>';//listaelemek hozzáfűzése a listához

}
$output .= '</ul>';//lista zárása
//kiírás egy lépésben
echo $output;

//13. ki fizette eddig a legtöbbet?
/*$qry = "SELECT
	customernumber cn
    FROM payments
    GROUP BY cn
    ORDER BY SUM(amount) DESC
    LIMIT 1;";//141*/

//SELECT customername FROM customers WHERE customernumber = 141;

//Belső select
/*$qry = "SELECT
       customername
        FROM customers
        WHERE customernumber = (SELECT
                                    customernumber cn
                                FROM payments
                                GROUP BY cn
                                ORDER BY SUM(amount) DESC
                                LIMIT 1)";*/

//'Kézi drótozás'
$qry = "SELECT 
        c.customerNumber cnr,
        customername cn,
        SUM(amount) total
        FROM 
            customers c,
            payments p
        WHERE	c.customerNumber = p.customerNumber
        GROUP BY c.customerNumber
        ORDER BY total DESC
        LIMIT 1;";

$result = mysqli_query($link,$qry) or die(mysqli_error($link));
$row = mysqli_fetch_assoc($result);
echo '<pre>';
var_dump($row);
echo '</pre>';
$output = "Vevőazonosító: {$row['cnr']}        | név: <b>{$row['cn']}</b> | Összeg: USD ";
//összeg formáázása és fűzése
$output .= number_format($row['total'],2, thousands_separator: ' ').'.-';//named parameter
//14. mennyit? ^^^^
echo $output;


//3. 	Melyik vevő rendelte eddig a legtöbbet (1:db -quantityordered vagy order darabszám, 2:érték)?
//---------------quantityordered össz darabszám
/*SELECT
        c.customernumber cnr,
        customername cn,
        COUNT(ordernumber) db
FROM
	customers c
LEFT JOIN orders o
ON	c.customerNumber = o.customerNumber
GROUP BY c.customerNumber
ORDER BY db DESC
LIMIT  1;*/
//2:érték szerinti

$qry = "SELECT 
            c.customernumber cnr,
            customername cn,
            SUM(quantityordered*priceeach) total
        FROM 
            customers c
        LEFT JOIN orders o
            ON	c.customerNumber = o.customerNumber
        LEFT JOIN orderdetails od
            ON o.orderNumber = od.orderNumber
        WHERE status != 'cancelled'
        GROUP BY c.customerNumber
        ORDER BY total DESC
        LIMIT 1";

$result = mysqli_query($link,$qry) or die(mysqli_error($link));
$row = mysqli_fetch_assoc($result);
echo '<pre>';
var_dump($row);
echo '</pre>';
$output = "Vevőazonosító: {$row['cnr']}        | név: <b>{$row['cn']}</b> | Összeg: USD ";
//összeg formáázása és fűzése
$output .= number_format($row['total'],2, thousands_separator: ' ').'.-';//named parameter
//14. mennyit? ^^^^
echo $output;

//----quantityorered alapján +1 tábla csatolással
//---------------megrendelések száma alapján lista:
