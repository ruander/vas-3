<?php

$a = filter_input(INPUT_GET,'a', FILTER_VALIDATE_INT);

switch ($a) {

    case 1:
        echo '1';
        //...
        break;

    case 2:
        echo '2';
        //...
        //break;
    case 3:
        echo '3';
        //...
        break;
    case 4 || 5:
        echo '4 vagy 5';
        //...
        break;

    default:
        echo 'default';
        break;
}
//visszatérés:
$match = match($a){
    1 => '1',
    2,3,4 => '2 vagy 3 vagy 4',
    default => 'default'
};
var_dump($match);
