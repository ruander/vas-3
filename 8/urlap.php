<?php
if (!empty($_POST)) {
    $errors = [];//hibatömb

    //név (min 3 karakter)
    $name = filter_input(INPUT_POST, 'name');
    $name = trim($name);//spacek levágása //ltrim,rtrim
    $name = strip_tags($name, ['b']);//tagek eltávolítása
    if (mb_strlen($name) < 3) {
        $errors['name'] = '<span class="error">Nem érvényes formátum!</span>';
    }

    //email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $errors['email'] = '<span class="error">Nem érvényes formátum!</span>';
    }

    //jelszó 1 és //jelszó2
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');

    if (mb_strlen($password) < 6) {//min 6 karakter.
        $errors['password'] = '<span class="error">Minimum 6 karakter!</span>';
    } elseif ($password !== $repassword) {//egyeznek-e
        $errors['repassword'] = '<span class="error">A jelszavak nem egyeztek!</span>';
    }else{
        //jelszó elkodólása (md5 -el NE!)

        $password = password_hash($password,PASSWORD_BCRYPT);

    }


    if (empty($errors)) {
        //adatok rendberakása
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'time_created' => date('Y-m-d H:i:s')
        ];

        //tároljuk el egy txt file-ba
        $fileName = 'testuser.txt';

        //data kiírása file-ba
        file_put_contents( $fileName , implode(',',$data) );


        var_dump('<pre>',$data);
        die();
    }
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Regisztráció</title>
    <style>
        label {
            display: flex;
            flex-direction: column;
        }

        .error {
            padding: 5px 0;
            color: red;
            font-size: .8em;
            font-style: italic;
        }
    </style>
</head>
<body>
<?php
//űrlap összeállítása (PURE PHP)
$form = '<form method="post">';//űrlap nyitása

//név mező
$form .= '<label>
            <span>Név <sup>*</sup></span>
            <input type="text" name="name" placeholder="Minta István" value="' . filter_input(INPUT_POST, 'name') . '">';
//hiba, ha van, hozzáfűzzük a mezőhöz
$form .= $errors['name'] ?? '';
$form .= '</label>';

//email
$form .= '<label>
            <span>Email <sup>*</sup></span>
            <input type="text" name="email" placeholder="email@cim.hu" value="' . filter_input(INPUT_POST, 'email') . '">';
//hiba, ha van, hozzáfűzzük a mezőhöz
$form .= $errors['email'] ?? '';
$form .= '</label>';

//jelszó (min 6 karakter)
$form .= '<label>
            <span>Jelszó <sup>*</sup></span>
            <input type="password" name="password" placeholder="******" value="">';
//hiba, ha van, hozzáfűzzük a mezőhöz
$form .= $errors['password'] ?? '';
$form .= '</label>';
//jelszó mégegyszer (egyeznie kell a másikkal)
$form .= '<label>
            <span>Jelszó újra <sup>*</sup></span>
            <input type="password" name="repassword" placeholder="******" value="">';
//hiba, ha van, hozzáfűzzük a mezőhöz
$form .= $errors['repassword'] ?? '';
$form .= '</label>';

$form .= '<button>Mehet</button>
</form>';//küldés és űrlap zárása

//kiírás 1 lépésben:
echo $form;
?>


</body>
</html>
