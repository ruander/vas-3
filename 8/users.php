<?php
//echo '<pre>'.var_export($_SERVER, true).'</pre>';
//erőforrások
/** @var mysqli $link */
require "connect.php";//db csatlakozás

$action = filter_input(INPUT_GET, 'action') ?? 'list';//action urlből
$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) ?? false;//id urlből
$title = 'Felhasználók';
$output = '';//ide gyűjtjük a kimenetet amit a végén 1 lépésben kiírunk

//űrlap feldolgozása
if (!empty($_POST)) {
    $errors = [];//hibatömb

    //név (min 3 karakter)
    $name = filter_input(INPUT_POST, 'name');
    $name = trim($name);//spacek levágása //ltrim,rtrim
    $name = strip_tags($name);//tagek eltávolítása

    //$name = strip_tags( trim( filter_input(INPUT_POST, 'name')));
    if (mb_strlen($name) < 3) {
        $errors['name'] = '<span class="error">Nem érvényes formátum!</span>';
    }

    //email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $errors['email'] = '<span class="error">Nem érvényes formátum!</span>';
    } else {
        //meg kell nézni nem foglalt-e az adatbázisban
        $qry = "SELECT id FROM users WHERE email = '$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        if ($row !== NULL && $row[0] != $id) {
            $errors['email'] = '<span class="error">Már foglalt email!</span>';
        }
    }
    //status, most fix 1, de jöhet majd űrlapból is
    $status = filter_input(INPUT_POST, 'status') ? 1:0;

    //jelszó 1 és //jelszó2
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');
    //jelszó ellenőrzés create esetben és módosításkor ha legalább 1 karaktert gépeltek a 1 jelszó mezőbe
    if ($action === 'create' || mb_strlen($password, 'utf-8') > 0) {
        if (mb_strlen($password) < 6) {//min 6 karakter.
            $errors['password'] = '<span class="error">Minimum 6 karakter!</span>';
        } elseif ($password !== $repassword) {//egyeznek-e
            $errors['repassword'] = '<span class="error">A jelszavak nem egyeztek!</span>';
        } else {
            $password = password_hash($password, PASSWORD_BCRYPT);
        }
    }

    if (empty($errors)) {
        //adatok rendberakása
        $data = [
            'name' => $name,
            'email' => $email,
            'status' => $status
        ];

        if ($action === 'create') {
            $data['time_created'] = date('Y-m-d H:i:s');
            $data['password'] = $password;

            $qry = "INSERT INTO 
            users(
                  name,
                  email,
                  password,
                  status,
                  time_created
                  ) 
            VALUES(
                   '{$data['name']}',
                   '{$data['email']}',
                   '{$data['password']}',
                   '{$data['status']}',
                   '{$data['time_created']}'
                   )";

        } else {
            //update ág
            $data['time_updated'] = date('Y-m-d H:i:s');
            //jelszó kiegészítés ha kell
            $passwordUpdate = $password != '' ? " password = '$password', " : '';
            $qry = "UPDATE users 
                    SET 
                        name = '{$data['name']}', 
                        email = '{$data['email']}',
                        $passwordUpdate
                        status = '{$data['status']}',
                        time_updated = '{$data['time_updated']}'
                    WHERE id = $id";
        }
        //irjuk db-be az adatokat

        mysqli_query($link, $qry) or die(mysqli_error($link));
        //visszakapott azonosító
        $newId = mysqli_insert_id($link);
        //var_dump($newId);
        //echo '<pre>'.var_export($_SERVER, true).'</pre>';
        header('location:' . $_SERVER['PHP_SELF']);
        exit;
    }
}

$output .= "<h2>$title</h2>";
switch ($action) {
    case 'delete':
        if ($id) {//ha kapunk id-t törlünk
            mysqli_query($link, "DELETE FROM users WHERE id  = $id LIMIT 1") or die(mysqli_error($link));
        }
        header('location:' . $_SERVER['PHP_SELF']);
        exit;
    //break;

    case 'update':
        $rowUser = [];
        if ($id) {
            $qryUser = "SELECT * FROM users WHERE id = $id LIMIT 1";
            $result = mysqli_query($link, $qryUser) or die(mysqli_error($link));
            $rowUser = mysqli_fetch_assoc($result);
        }
        echo '<pre>' . var_export($rowUser, true) . '</pre>';

    //break;

    case 'create':
        $output .= '<a href="?">vissza</a>';

        //űrlap összeállítása (PURE PHP)
        $form = '<form method="post">';//űrlap nyitása

        //név mező
        $form .= '<label>
            <span>Név <sup>*</sup></span>
            <input type="text" name="name" placeholder="Minta István" value="';
        // shorten if:  condition ? true : false;
        $form .= filter_input(INPUT_POST, 'name') ?: $rowUser['name'] ?? '';
        /*
         if( filter_input(INPUT_POST, 'name') ){
            $form .= filter_input(INPUT_POST, 'name')
        }elseif(isset($rowUser['name'])){
            $form .= $rowUser['name']
        }else{
            $form .= '';
        }
         * */

        $form .= '">';
        //hiba, ha van, hozzáfűzzük a mezőhöz
        $form .= $errors['name'] ?? '';
        $form .= '</label>';

        //email
        $form .= '<label>
            <span>Email <sup>*</sup></span>
            <input type="text" name="email" placeholder="email@cim.hu" value="' . (filter_input(INPUT_POST, 'email') ?: $rowUser['email'] ?? '') . '">';
//hiba, ha van, hozzáfűzzük a mezőhöz
        $form .= $errors['email'] ?? '';
        $form .= '</label>';

//jelszó (min 6 karakter)
        $form .= '<label>
            <span>Jelszó <sup>*</sup></span>
            <input type="password" name="password" placeholder="******" value="">';
//hiba, ha van, hozzáfűzzük a mezőhöz
        $form .= $errors['password'] ?? '';
        $form .= '</label>';
//jelszó mégegyszer (egyeznie kell a másikkal)
        $form .= '<label>
            <span>Jelszó újra <sup>*</sup></span>
            <input type="password" name="repassword" placeholder="******" value="">';
//hiba, ha van, hozzáfűzzük a mezőhöz
        $form .= $errors['repassword'] ?? '';
        $form .= '</label>';

        //status checkbox beillesztése
        //a valuet fixen kell megadni (1)
        //ha aktív a checkbox akkor létezik az elem a POSTBAN
        //ha inaktív a checkbox akkor nem létezik az elem a POSTBAN
        //első betöltéskor (ha nincs post adat) akkor az adatbázis szerint pipáld ki, ha van post adat akkor mindenképpen az alapján
        #checkbox kezelése
        //Teszt eset: Aktív felhasználó esetén alapból ki van pipálva
        //inaktívvá akarjuk tenni, DE közben hibát generálunk egy másik mezőn ezért
        //hibaüzennettel betöltődik újra az űrlap de ebben az esetbe inaktívnak kell maradni a status checkboxnak, mert az utolsó művelet ott az volt, hogy kivettük a pipát
        /*$checked = '';
        if(filter_input(INPUT_POST,'status')){
            $checked = 'checked';
        }*/
        //rövid if-el:
        //$checked = filter_input(INPUT_POST, 'status') ? 'checked' : '';
        /*
        a) A checkbox aktív, ha
                első betöltés van (nincs post adat) és az adatbázisból 1 értéket kapunk status-ra
                   vagy
                ha a postban megtalálható a status elem

        b) A checkbox inaktív, ha
                első betöltés van (nincs post adat) de az adatbázisból 0 értéket kapunk status-ra
                    vagy
                van postadat ,de nincs köztük a status elem
         */
        $checked = '';
        $active = isset($rowUser['status']) ? $rowUser['status'] : 0;
        $active = $rowUser['status'] ?? 0;
        if(
            empty($_POST)
                &&
            $active  == 1
            ||
            filter_input(INPUT_POST,'status')
        ){
            $checked = 'checked';
        }
        //b)
/*        $checked = 'checked';
        if(
            empty($_POST) && ($rowUser['status'] ?? 0) == 0
                ||
            !empty($_POST) && filter_input(INPUT_POST, 'status' == false)
        ){
            $checked = '';
        }*/

        $form .= '<label>
            
            <input type="checkbox" name="status" value="1" ' . $checked . '>';
//hiba, ha van, hozzáfűzzük a mezőhöz
        $form .= $errors['repassword'] ?? '';
        $form .= '</label>';

        $btnTitle = $action === 'create' ? 'Új felvitel' : 'Módosítás';

        $form .= '<button>' . $btnTitle . '</button>
             </form>';//küldés gomb és űrlap zárása
        //urlap->output
        $output .= $form;
        break;

    default:
        //adattábla lekérése
        $qry = "SELECT id,name,email,status FROM users";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));

        $table = ' <a href="?action=create">Új felvitel</a>
           <table border="1">';//tábla nyitása
        $table .= '<tr>
              <th>ID</th>
              <th>név</th>
              <th>email</th>
              <th>státusz</th>
              <th>művelet</th>
            </tr>';

//sorok ciklusból
        while (($row = mysqli_fetch_assoc($result)) !== null) {
            $table .= '<tr>
                  <td>' . $row['id'] . '</td>
                  <td>' . $row['name'] . '</td>
                  <td>' . $row['email'] . '</td>
                  <td>' . $row['status'] . '</td>
                  <td> 
                      <a href="?action=update&amp;id=' . $row['id'] . '">szerkeszt</a>   
                      <a href="?action=delete&amp;id=' . $row['id'] . '" onclick="return confirm(\'Are you sure you want to delete this item\')">töröl</a> 
                  </td>
              </tr>';
        }
        $table .= '</table>';

        $output .= $table;//table->output
        break;
}
/**
 * @todo: HF: törlés megerősítése javascript confirmmel
 * $todo: HF2 Adatbázis táblaterv. Egy cikk, vagy bejegyzés eltárolása (articles)
 */

//output kiírása
echo $output;
echo '<style>
    label {
    display: block;
    }
</style>';
