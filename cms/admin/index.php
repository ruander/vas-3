<?php
//Erőforrások
//adatbázis csatlakozás
include "../config/connect.php";/** @var mysqli $link */
include "../config/settings.php";//bállítások
include "../config/functions.php";//saját eljárások
session_start();//munkafolyamat inditása
$output = '';//ide gyűjtöm a kiírandó elemeket
if(filter_input(INPUT_GET,'logout') !== NULL){
    logout();
}

$auth = auth();//ellenőrzés
if($auth === false){
    header('location:login.php');
    exit;
}

//modul betöltése ha van
$moduleName = 'users';
$modulePath = MODULE_DIR.$moduleName.MODULE_EXT;
//töltsük be a modult ha létezik
if(file_exists($modulePath)){
    include $modulePath;
}else{
    $output = "Nincs ilyen modul: $modulePath";
}


$userBar = '<header>Üdvözzölek '.$_SESSION['userdata']['name'].'! | <a href="?logout=true">kilépés</a></header>';
echo $userBar;

echo $output;//ez a modulból érkezik
