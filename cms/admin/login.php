<?php
//Jogosultság ellenőrzése (most csa annyi elég hogy a users táblában 1 státusszal szerepel
//$auth = true -> ezt kell elérni, viszont a vizsgálata egy másik fileban lesz
//erre megoldás a Munkafolyamat alkalmazása

//Erőforrások
//adatbázis csatlakozás
include "../config/connect.php";/** @var mysqli $link */
include "../config/settings.php";//bállítások
include "../config/functions.php";//saját eljárások
session_start();//munkafolyamat inditása
$info = 'Írja be a bejelentkezési adatokat:';

if (!empty($_POST)) {
    if (login()) {
        //minden ok ...
        header('location:index.php');
        exit;
    } else {
        $info = '<div class="error">Érvénytelen email/jelszó páros!</div>';
    }
}


?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Egyszerű CMS - Bejelentkezés</title>
</head>
<body>
<form method="post">
    <?php echo $info ?>
    <label>
        <span>email</span>
        <input type="text" name="email" placeholder="email@cim.hu"
               value="<?php echo filter_input(INPUT_POST, 'email'); ?>">
    </label>
    <label>
        <span>jelszó</span>
        <input type="password" name="password" placeholder="******" value="">
    </label>
    <button>Belépés</button>
</form>
</body>
</html>
