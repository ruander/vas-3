<?php
//Adatbázis elérés kialakítása

$dbHost = 'localhost';//adatbázis szerver ip vagy domain általában localhost
$dbUser = 'root';//xamp teljes jogú felh.
$dbPassword = '';
$dbName = 'php_tanfolyam_vas';//ebbe az adatbázisba szeretnénk dolgozni

//kapcsolat felépítése (mysqli procedurális módszerrel) - erőforrás amin tudsz futtatni mysqli_ parancsokkal sql kéréseket
$link = @mysqli_connect($dbHost , $dbUser, $dbPassword, $dbName) or die(mysqli_connect_error().'<br>Keresse a rendszergizdát, mert baj van!');

//kódlap illesztése
mysqli_set_charset($link,'utf8');
