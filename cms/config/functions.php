<?php
//saját eljárások (helperek)
//beléptetés funkció
function login(): bool
{
    //a metódus idejére globálissá teszük a $link változót
    global $link;

    $email = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'email'));
    $password = filter_input(INPUT_POST, 'password');

    //kérjük le az emailhez tartozó jelszót (és ha már lekérünk akkor a szükséges elemeket is
    $qry = "SELECT id,name, password FROM users WHERE email = '$email' AND status = 1 LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));

    $rowUser = mysqli_fetch_assoc($result);//vagy megvannak az adatok vagy null

    if ($rowUser && password_verify($password, $rowUser['password'])) {
        //jelszo oké
        $sid = session_id();//mf azonosító
        $spass = md5($rowUser['id'] . $sid . SECRET_KEY);//mf jelszó
        $stime = $now = time();//mf idő (timestamp)

        //beragadt mf azonosító törlése
        mysqli_query($link, "DELETE FROM sessions WHERE sid = '$sid' LIMIT 1") or die(mysqli_error($link));

        //belépés adatainak tárolása
        $qry = "INSERT  INTO sessions(sid,spass,stime) VALUES('$sid','$spass', $stime)";
        mysqli_query($link, $qry) or die(mysqli_error($link));

        //tároljuk el a felh adatait a mf idejére
        $_SESSION = [
            'id' => $sid,
            'userdata' => [
                'id' => $rowUser['id'],
                'name' => $rowUser['name'],
                'email' => $email
            ]
        ];
        //var_dump($_SESSION);
        return true;
    }
    return false;
}

/**
 * Belépés meglétének ellenőrzése (authentication)
 */
function auth(): bool
{
    global $link;

    $now = time();
    $expired = $now - 15*60;//
//öntisztítás , lejárt mf ok törlése:
    mysqli_query($link, "DELETE FROM sessions WHERE stime < $expired") or die(mysqli_error($link));
//aktuális session_id
    $sid = session_id();
//kérjük le az aktuális mf hoz tartozó rekordot, ha van
    $qry = "SELECT spass FROM sessions WHERE sid = '$sid' AND stime > $expired LIMIT 1";
    $result = mysqli_query($link,$qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);
//állítsuk össze újra a spasst a rendelkezésre álló adatokból
    $spass = md5($_SESSION['userdata']['id'].$_SESSION['id'].SECRET_KEY);

    if($row && $spass === $row[0]){
        //ok -stime update
        mysqli_query($link,"UPDATE sessions SET stime = $now WHERE sid = '$sid' LIMIT 1") or die(mysqli_error($link));
        return true;
    }

    return false;
}

/**
 * Kiléptetés
 * @return void
 */
function logout(): void
{
    global $link;
    //kilépés ha kell
    //db rekord törlése
    mysqli_query($link,"DELETE FROM sessions WHERE sid = '".session_id()."' ") or die(mysqli_error($link));
    $_SESSION = [];//mf tömb ürítése
    session_destroy();//mf roncsolása
}
